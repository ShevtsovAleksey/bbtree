#include <stdio.h>
#include <stdlib.h>
#include "Array.h"

int initArray(Array *a, int initialSize) {
  a->array = (node **)malloc(initialSize * sizeof(node));
  if(a->array == NULL) {
    printf("out of memory\n");
    return -1;
  }
  a->used = 0;
  a->size = initialSize;
  return 0;
}

int insertArray(Array *a, node* element) {
  if (a->used == a->size) {
    a->size *= 2;
    a->array = (node **)realloc(a->array, a->size * sizeof(node));
    if( a->array == NULL ) {
      printf("out of memory\n");
      return -1;
    }
  }
  a->array[a->used++] = element;
  return 0;
}

void freeArray(Array *a) {
  free(a->array);
  a->array = NULL;
  a->used = a->size = 0;
}
