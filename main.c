#include <stdio.h>
#include "BTReeBuilder.h"
#include "TreePrinter.c"

node* nod = NULL;

void traversNod(node* root) {
  if (root) {
    nod = insert(root->value, nod);
    traversNod(root->left);
    traversNod(root->right);
  }
}

int main() {
  
  FILE* file;
  FILE* outFile;
  file = fopen("data.txt","r");
  outFile = fopen("out.txt","w");
  
  if (file == NULL) {
    printf("Error! opening file");
    return 1;
  }
  
  node* tree =  readTreeFromFile(file);
  traversNod(tree);
  
  printf("\n\nBase Tree:\n\n");
  printTree(tree, 0);
  printf("\n\nBase Tree Paths:\n\n");
  printPaths(tree, NULL);
  
  printf("\n\nBalanced Tree:\n\n");
  printTree(nod, 0);
  printf("\n\nBase Tree Paths:\n\n");
  fprintf(outFile, "\n\n\nBalanced Tree:\n\n");
  printPaths(nod, outFile);
  
  fclose(file);
  return 0;
}
