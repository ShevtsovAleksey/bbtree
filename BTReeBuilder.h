#ifndef __BBTreeBulder__BTreeBulder__
#define __BBTreeBulder__BTreeBulder__

#include <stdio.h>
#include "BTree.h"
#include "Array.h"

void printTree(node * p,int level);
void preOrderTravers(node* root, node* n, int * counter );
void addNodeToTree(node* t, node* n, int* counter);
void replaceMarker(char* buffer);
void traversNod(node* root);
node* readTreeFromFile(FILE *fptr);

#endif /* defined(__BBTree__BTree__) */
