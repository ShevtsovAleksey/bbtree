#include <stdlib.h>
#include <stdio.h>
#include "BTree.h"

void printArray(int ints[], int len, FILE* file) {
  int i;
  for (i=0; i<len; i++) {
    printf("%d ", ints[i]);
    if(file != NULL) {
      fprintf(file, "%d ", ints[i]);
    }
  }
  printf("\n");
  if(file != NULL) {
    fprintf(file, "\n ");
  }
}

void printPathsRecur(struct node* node, int path[], int pathLen, FILE* file) {
  if (node==NULL)
    return;
  
  path[pathLen] = node->value;
  pathLen++;
  
  if (node->left==NULL && node->right==NULL) {
    printArray(path, pathLen, file);
  } else {
    printPathsRecur(node->left, path, pathLen, file);
    printPathsRecur(node->right, path, pathLen, file);
  }
}

void printPaths(struct node* node, FILE* file) {
  int path[1000];
  if(file != NULL) {
    printPathsRecur(node, path, 0, file);
    return;
  }
  printPathsRecur(node, path, 0, NULL);
}