#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include "BTReeBuilder.h"

void printTree(node* t, int level) {
  if(t) {
    printTree(t->left, level + 1);
    for (int i = 0; i < level; i++) {
      printf("   ");
    }
    printf("%d \n", t->value );
    printTree(t->right, level + 1);
  }
}

void travers(node* t, node* n, int* counter) {
  if (t) {
    if (t->left == NULL && t->right == NULL && n->value == t->value) {
      t->left = n->left;
      t->right = n->right;
      (*counter)++;
    } else {
      travers(t->left, n, counter);
      travers(t->right, n, counter);
    }
  }
}

void addNodeToTree(node* t, node* n, int* counter) {
  if( n->left != NULL && t->value == n->left->value ){
    *n->left = *t;
    *t = *n;
    (*counter)++;
    return;
  }else if(n->right != NULL && t->value == n->right->value){
    (*n->right) = (*t);
    (*t) = (*n);
    (*counter)++;
    return;
  }
  travers(t, n, counter);
}

node* parseNodeFromString(char *buffer) {
  node* n = NULL;
  int int1 , int2, int3;
  replaceMarker(buffer);
  if (sscanf (buffer, "%d %d %d", &int1, &int2, &int3)  > 1) {
    n = (node*)malloc(sizeof(node));
    n->value = int1;
    n->right = NULL;
    n->left = NULL;
    if (int2 != 0) {
      n->right = (node*)malloc(sizeof(node));
      n->right->value = int2;
      n->right->left = NULL;
      n->right->right = NULL;
    }
    if (int3 != 0) {
      n->left = (node*)malloc(sizeof(node));
      n->left->value = int3;
      n->left->left = NULL;
      n->left->right = NULL;
    }
  }
  return n;
}


void replaceMarker(char* buffer) {
  for (int i = 0; i < strlen(buffer) ; i++) {
    if (buffer[i] == '-') {
      buffer[i] = '0';
    }
  }
}

node* readTreeFromFile(FILE *fptr) {
  char buffer[100];
  Array array;
  if(initArray(&array, 10)) {
    return NULL;
  }
  
  int oldCount = 0, counter = 0;
  node* n, *root = NULL;

  while (fgets( buffer, 100, fptr ) != NULL) {   
    n = parseNodeFromString(buffer);
    if (n != NULL){
      if(root == NULL){
        root = n;
      }else{
        oldCount = counter;
        addNodeToTree(root, n, &counter);
        if (oldCount == counter){
          insertArray(&array, n);
        }
      }
    }
  }

  int arrayCount = array.used, arrayOldCount = 0;
  while(arrayCount > 0 && arrayCount != arrayOldCount) {
    arrayOldCount = arrayCount;
    //printf("arrayCount %d\n", arrayCount);
    for (int i = 0; i < array.used; i++) {
      if (array.array[i] != NULL){
        oldCount = counter;
        addNodeToTree(root, array.array[i], &counter);
        if (counter == oldCount + 1) {
          node*  w = array.array[i];
          //printf("arr %d\n",w->value);
          array.array[i] = NULL;
          arrayCount--;
        }
      }
    }
  }
  
  return root;
}
