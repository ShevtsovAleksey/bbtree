#ifndef __BArray__Array__
#define __BArray__Array__

#include "BTree.h"

typedef struct {
  node** array;
  int used;
  int size;
} Array;

int initArray(Array *a, int initialSize);
int insertArray(Array *a, node* element);
void freeArray(Array *a);


#endif /* defined(__BArray__Array__) */