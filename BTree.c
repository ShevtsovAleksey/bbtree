#include <stdio.h>
#include <stdlib.h>
#include "BTree.h"

void dispose( node* n ) {
	if( n != NULL )	{
		dispose( n->left );
		dispose( n->right );
		free( n );
	}
}

node* find( int e, node* n ) {
	if( n == NULL ) {
		return NULL;
	}
	if( e < n->value ) {
		return find( e, n->left );
	}	else if( e > n->value ) {
		return find( e, n->right );
	}	else {
		return n;
	}
}

node* find_min( node* n ) {
	if( n == NULL ) {
		return NULL;
	}	else if( n->left == NULL ) {
		return n;
	} else {
		return find_min( n->left );
	}
}

node* find_max( node* n ) {
	if( n != NULL ) {
		while( n->right != NULL ) {
			n = n->right;
		}
	}
	return n;
}

static int height( node* n ) {
	if( n == NULL ) {
		return -1;
	}	else {
		return n->height;
	}
}

static int max( int l, int r) {
	return l > r ? l : r;
}

static node* single_rotate_with_left( node* k2 ) {
	node* k1 = NULL;
	k1 = k2->left;
	k2->left = k1->right;
	k1->right = k2;
 
	k2->height = max( height( k2->left ), height( k2->right ) ) + 1;
	k1->height = max( height( k1->left ), k2->height ) + 1;
	return k1; /* new root */
}

//only if the k1 node has a right child
static node* single_rotate_with_right( node* k1 ) {
	node* k2;
	k2 = k1->right;
	k1->right = k2->left;
	k2->left = k1;
 
	k1->height = max( height( k1->left ), height( k1->right ) ) + 1;
	k2->height = max( height( k2->right ), k1->height ) + 1;
	return k2;  /* New root */
}

//only if k3 node has a left child and k3's left child has a right child
static node* double_rotate_with_left( node* k3 ) {
	/* Rotate between k1 and k2 */
	k3->left = single_rotate_with_right( k3->left );
	/* Rotate between K3 and k2 */
	return single_rotate_with_left( k3 );
}

//only if k1 has a right child and k1's right child has a left child
static node* double_rotate_with_right( node* k1 ) {
	/* rotate between K3 and k2 */
	k1->right = single_rotate_with_left( k1->right );
	/* rotate between k1 and k2 */
	return single_rotate_with_right( k1 );
}

node* insert( int e, node* n ) {
	if( n == NULL )	{
		n = (node*)malloc(sizeof(node));
		if( n == NULL ) {
			fprintf (stderr, "Out of memory!!! (insert)\n");
			exit(1);
		} else {
			n->value = e;
			n->height = 0;
			n->left = n->right = NULL;
		}
	} else if( e < n->value ) {
		n->left = insert( e, n->left );
		if( height( n->left ) - height( n->right ) == 2 ) {
			if( e < n->left->value ) {
				n = single_rotate_with_left( n );
			}	else {
				n = double_rotate_with_left( n );
			}
		}
	}	else if( e > n->value ) 	{
		n->right = insert( e, n->right );
		if( height( n->right ) - height( n->left ) == 2 ) {
			if( e > n->right->value ) {
				n = single_rotate_with_right( n );
			} else {
				n = double_rotate_with_right( n );
			}
		}
	}
	/* Else x is in the tree already - do nothing */
	n->height = max( height( n->left ), height( n->right ) ) + 1;
	return n;
}

node* delete( int e, node* n ) {
	printf( "Sorry; Delete is unimplemented; %d remains\n", e );
	return n;
}

int get(node* n) {
	return n->value;
}

void display_avl(node* n) {
	if (n == NULL) {
		return;
	}
	printf("%d",n->value);
	
	if(n->left != NULL) {
		printf("(L:%d)",n->left->value);
	}
	if(n->right != NULL) {
		printf("(R:%d)",n->right->value);
	}
	printf("\n");
 
	display_avl(n->left);
	display_avl(n->right);
}
