#ifndef __BBTree__BTree__
#define __BBTree__BTree__

typedef struct node {
  int value;
  int height;
  struct node* left;
  struct node* right;
} node;


void dispose( node* t );
node* insert( int data, node *t );
node* delete( int data, node *t );
void display_avl( node* t );
int get( node* n );

#endif /* defined(__BBTree__BTree__) */
